#!/bin/bash

## Surveillance des logs, et envoi d'un SMS quand un utilisateur se connecte ou déconnecte.

## Add the following line to /etc/pam.d/sshd
# session optional pam_exec.so seteuid /opt/scripts/logs-sms.sh

logger "SMS: Démarrage script SMS connexions."

envoie_sms () {
   chaine="[ `date +%Y-%m-%d_%Hh%M` $*"
   logger "SMS: $chaine"
   curl -G -d user=ChangeMe -d pass=ChangeMe --data-urlencode msg="$chaine" 'https://smsapi.free-mobile.fr/sendmsg'
}

if [ "$PAM_TYPE" = "open_session" ]; then
    host="`hostname`"
    subject="] SSH [ LOGIN: $PAM_USER ] - [ FROM: $PAM_RHOST ] - [ ON: $host ]"
    envoie_sms "$subject"
fi
